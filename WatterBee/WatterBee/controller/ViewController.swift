//
//  ViewController.swift
//  WatterBee
//
//  Created by Patrick Magalhães de Lima on 18/09/15.
//  Copyright © 2015 Patrick Magalhães de Lima. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView!
    var datas = [Data]()
    
    var refreshControl:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl)
        
        getData()
    }
    
    func refresh(sender:AnyObject)
    {
        datas.removeAll()
        getData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*TABLE VIEW*/
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("BasicCell")
        cell!.textLabel?.text = String(self.datas[indexPath.row].measurement)
        cell!.detailTextLabel?.text = String(self.datas[indexPath.row].device)
        
        return cell!
    }
    
    func getData(){
        HttpRequest.getJSON("http://\(HttpRequest.URL)/WaterSolution/ws.php/datas") {
            (data: Dictionary<String, AnyObject>, error: String?) -> Void in
            if error != nil {
                /*TIMED OUT*/
                print(error)
            } else {
                if let entries = data["datas"] as? NSArray{
                    var tempDatas = [Data]()
                    for elem: AnyObject in entries {
                        if let dict = elem as? NSDictionary,
                            let id = dict["dat_id"] as? String,
                            let measurement = dict["dat_measurement"] as? String,
                            let device = dict["dat_device"] as? String {
                                let dt = Data(id:id,measurement:Float(measurement)!,device:device)
                                    tempDatas.append(dt)
                        }
                    }
                    self.datas = tempDatas
                }
                dispatch_async(dispatch_get_main_queue(),{
                    self.tableView.reloadData()
                    if(self.refreshControl.refreshing) {
                        self.refreshControl.endRefreshing()
                    }                    
                })
            }
        }
    }
    
}

