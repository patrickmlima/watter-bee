//
//  Data.swift
//  WatterBee
//
//  Created by Patrick Magalhães de Lima on 18/09/15.
//  Copyright © 2015 Patrick Magalhães de Lima. All rights reserved.
//

import Foundation

class Data {
    
    var id = String()
    var measurement = Float()
    var device = String()
    
    init(){}
    
    init(id:String,measurement:Float,device:String){
        self.id = id
        self.measurement = measurement
        self.device = device;
    }
}